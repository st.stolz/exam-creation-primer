# Exam Creation Primer

Dieses Projekt dient als Vorlage, um einfach Prüfungsfragen mit Lösungen in Markdown ausarbeiten zu können. Es werden automatisch zwei PDFs erstellt. Eines für die Lösung und eines für die Angabe.

## Voraussetzung

Docker muss am Computer laufen. Unter Windows ist also Docker Desktop Voraussetzung. 

## Starten

1. In vscode die Extension ms-vscode-remote.remote-containers installieren. Oder einfach das Projekt in vscode öffnen. Habe ich so eingestellt, dass sie vorgeschlagen wird.
2. Wenn remote-containers installiert ist, am einfachsten noch einmal öffnen. Dann fragt er rechts unten, ob er die Container Umgebung starten soll. (Alternative > Dev Containers: Reopen in Container)
3. Jetzt einige Zeit warten... ca 3 Minuten bei mir
4. Sobald das Projekt gestartet ist, auf einen Ordner mit Fragen (gehen auch Überordner) "Rechts Klick" - "Open in integrated Terminal" und dann `exam_creation.bash` eingeben ("exam"-TAB). Dann erzeugt er die PDFs (immer eine normal und eine Lösung). Ich habe Beispiele reingepackt. 
5. Ich habe das Script so geschrieben, dass er immer die Files rekursive im darunter liegenden Ordner kompiliert.

## Funktionen

1. Code Blöcke sind ganz normales markdown.
2. Screenshots aus Zwischenablage einfügen: STRG-ALT-C im Markdown
3. Fragen Vorlage: `> Foam: Create New Template`. Neue Vorlagen einfach unter foam/templates anlegen.