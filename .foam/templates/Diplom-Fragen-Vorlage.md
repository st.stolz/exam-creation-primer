---
type: diplom-question
# tags: my-tag1 my-tag2
foam_template:
    description: Mündliche Diplomprüfungen
    #filepath: $FOAM_TITLE.md
header-includes:
  - \usepackage{fancyhdr}
  - \usepackage{lastpage}
  - \pagestyle{fancy}
  # - \fancyhead[RO,RE]{Rechts oben}
  # - \fancyhead[LO,LE]{Links oben}
  # - \fancyfoot[LE,LO]{Links unten}
  - \fancyfoot[LE,CO]{}
  - \fancyfoot[LE,RO]{\thepage\ of \pageref{LastPage}}
---

# Themenbereich ${1:Themenbereich Nr.} - Frage ${2:Frage Nr.} - ${3:Lehrer Kürzel}

${4:Bezeichnung Themenbereich} - $FOAM_TITLE

## Theorie (3P)

<!-- BEGIN SOLUTION -->

### Lösung

<!-- END SOLUTION -->

## Praxis (3P)

<!-- BEGIN SOLUTION -->

### Lösung

<!-- END SOLUTION -->

## Reflexion (3P)

<!-- BEGIN SOLUTION -->

### Lösung

<!-- END SOLUTION -->

## Anhang