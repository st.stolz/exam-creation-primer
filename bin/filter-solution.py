#!/usr/bin/env python

"""
Pandoc filter to convert all regular text to uppercase.
Code, link URLs, etc. are not affected.
"""

from pandocfilters import toJSONFilter, Str, Emph, Para, RawBlock
import re

incomment = False

def caps(key, value, format, meta):
    global incomment
    if key == 'RawBlock':
        format, s = value
        if format == "html" or format == "html5":
            if re.search("<!-- BEGIN SOLUTION -->", s):
                incomment = True
                return RawBlock('latex', '{ \\color{blue}')
            elif re.search("<!-- END SOLUTION -->", s):
                incomment = False
                return RawBlock('latex', '}')
    if incomment and key == 'Str':
        return Str(value)

if __name__ == "__main__":
    toJSONFilter(caps)