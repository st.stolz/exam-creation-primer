#!/usr/bin/env python
from pandocfilters import toJSONFilter
import re

"""
Pandoc filter that causes everything between
'<!-- BEGIN COMMENT -->' and '<!-- END COMMENT -->'
to be ignored.  The comment lines must appear on
lines by themselves, with blank lines surrounding
them.
"""

incomment = False


def comment(key, value, format, meta):
    global incomment
    if key == 'RawBlock':
        format, s = value
        if format == "html":
            if re.search("<!-- BEGIN SOLUTION -->", s):
                incomment = True
                return []
            elif re.search("<!-- END SOLUTION -->", s):
                incomment = False
                return []
    if incomment:
        return []  # suppress anything in a comment

if __name__ == "__main__":
    toJSONFilter(comment)