#!/bin/bash

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
filter_path=${SCRIPT_DIR}
file_ending="pdf"
if [ -n "$1" ]
then
    file_ending=$1
fi



if [ ! -d "${filter_path}" ]
then
    >&2 echo "Einen gültigen Pfad für die filter.py angeben!"
    exit 1
fi

# if [ ! -f "${file}" ]
# then
#     >&2 echo "Keine gültige Datei angegeben!"
#     exit 1
# fi

OIFS="$IFS"
IFS=$'\n'
for file_orig in $(find ${PWD} -type f -name '*.md'); do 
    echo "processing: ${file_orig}"
    dir_without_filename=$(dirname -- "$file_orig")
    filename=$(basename -- "$file_orig")
    extension="${filename##*.}"
    filename="${filename%.*}"
    file="${dir_without_filename}/${filename}_tmp.${extension}"
    echo $file
    awk '{for(x=1;x<=NF;x++)if($x~/Field_replace/){sub(/Field_replace/,"auto_number_"++i)}}1' "${file_orig}" > "${file}"
    workdir=${PWD}
    echo $workdir
    last_dir_name=$(basename -- "$dir_without_filename")
    file_without_path=$(basename -- "$file")
    extension="${file_without_path##*.}"
    filename="${file_without_path%.*}"
    (
    cd ${dir_without_filename}
    pandoc --pdf-engine=xelatex "${filter_path}/header.yaml" "${filter_path}/header-solution.md" "$file_without_path" -F "${filter_path}/filter-solution.py" -F "${filter_path}/pandoc_mermaid_filter.py" -o "${workdir}/${last_dir_name}-${filename}-solution.${file_ending}"
    pandoc --pdf-engine=xelatex "$file_without_path" -F "${filter_path}/filter-nosolution.py" -F "${filter_path}/pandoc_mermaid_filter.py" -o "${workdir}/${last_dir_name}-${filename}-nosolution.${file_ending}"
    )
    rm $file
done
IFS="$OIFS"

chown -R 1000:1000 *