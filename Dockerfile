
# ARG VARIANT="focal"
FROM qmcgaw/latexdevcontainer

RUN tlmgr update --self && \
    tlmgr install koma-script setspace import blindtext listings caption copyrightbox pgf units enumitem csquotes cleveref babel-german helvetic && \
    texhash

RUN apt-get update && apt-get install -y pandoc python3 python-is-python3 python3-pandocfilters texlive-xetex curl

## mermaid-cli

RUN apt-get install -y chromium libnss3 libgbm-dev libasound-dev

ARG USERNAME=splinter
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    #
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

RUN apt-get install -y npm
RUN npm cache clean -f 
RUN npm install -g n 
RUN n stable 
RUN npm install -g @mermaid-js/mermaid-cli

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn

RUN apt-get install -y pip
RUN pip install pandoc-mermaid-filter

COPY bin/* /usr/local/bin/
COPY . /app

# SHELL ["bash", "-c"]
RUN chsh -s /bin/bash root
RUN chsh -s /bin/bash $USERNAME

RUN yarn global add @mermaid-js/mermaid-cli@$VERSION

RUN ln -s /usr/local/share/.config/yarn/global/node_modules/.bin/mmdc /usr/local/bin/mermaid

ENV MERMAID_BIN=/usr/local/share/.config/yarn/global/node_modules/.bin/mmdc
ENV PUPPETEER_CFG=/app/puppeteer/puppeteer-config.json

RUN node /usr/local/share/.config/yarn/global/node_modules/puppeteer/install.js

# RUN echo 'alias exam_creation.bash="sudo -u splinter -E exam_creation.bash"' >> ~/.bashrc

USER $USERNAME

RUN node /usr/local/share/.config/yarn/global/node_modules/puppeteer/install.js

RUN echo 'alias exam_creation.bash="sudo exam_creation.bash"' >> ~/.bashrc

ENTRYPOINT ["/bin/bash"]