---
type: diplom-question
---

# Themenbereich 01 - Frage 01 - Lehrer Kürzel

Themenbereich Bezeichnung - 01 Genauere Fragenbezeichnung

## Theorie (3P)

1. Nulla neque dolor sagittis eget?
2. Morbi mollis tellus ac sapien?
3. Vestibulum turpis sem aliquet eget?

```mermaid
gantt
        dateFormat  YYYY-MM-DD
        title Adding GANTT diagram functionality to mermaid
        section A section
        Completed task            :done,    des1, 2014-01-06,2014-01-08
        Active task               :active,  des2, 2014-01-09, 3d
        Future task               :         des3, after des2, 5d
        Future task2               :         des4, after des3, 5d
        section Critical tasks
        Completed task in the critical line :crit, done, 2014-01-06,24h
        Implement parser and jison          :crit, done, after des1, 2d
        Create tests for parser             :crit, active, 3d
        Future task in critical line        :crit, 5d
        Create tests for renderer           :2d
        Add to mermaid                      :1d
```

<!-- BEGIN SOLUTION -->

### Lösung

1. Vestibulum fringilla pede sit amet
2. Aenean posuere tortor sed cursus
3. Praesent nonummy mi in odio

<!-- END SOLUTION -->

## Praxis (3P)

Etiam iaculis nunc ac metus. Mauris turpis nunc blandit et. Sed cursus turpis vitae tortor?

```yaml
version: '3.3'

services:
  db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_DATABASE: exampledb
      MYSQL_USER: exampleuser
      MYSQL_PASSWORD: examplepass
      MYSQL_RANDOM_ROOT_PASSWORD: '1'
    volumes:
      - db:/var/lib/mysql

volumes:
  db:
```

1. Praesent venenatis metus
2. Etiam ultricies nisi

![](attachments/2-subnets-routed.png)

<!-- BEGIN SOLUTION -->

### Lösung

1. Praesent venenatis metus
   1. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Nullam quis ante. Ut a nisl id ante tempus hendrerit. Cras id dui. Cras dapibus.
   2. Phasellus consectetuer vestibulum elit. Fusce ac felis sit amet ligula pharetra condimentum. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. Morbi ac felis.
2. Etiam ultricies nisi
   1. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Cras id dui. Morbi mollis tellus ac sapien.
   2. Cras non dolor. Curabitur blandit mollis lacus. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus.

<!-- END SOLUTION -->

## Reflexion (3P)

Donec venenatis vulputate lorem. In auctor lobortis lacus. Etiam feugiat lorem non metus. Fusce vel dui. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus?


<!-- BEGIN SOLUTION -->

### Lösung

1. Mauris sollicitudin fermentum libero. Nulla porta dolor. Cras non dolor. Nullam sagittis.
2. Ut leo. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Phasellus blandit leo ut odio. 
3. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.
4. Ut leo. Duis lobortis massa imperdiet quam. Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit. Nulla sit amet est.

<!-- END SOLUTION -->